/**
 * 数据库连接实例
 * Created by mcbird on 4/24/15.
 */

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/peer');

var db = mongoose.connection;

db.on('open', function(callback) {
	console.log('connected to peer Datebase successed!');
});
db.on('error', function(err) {
	console.error.bind(console, 'connection error:' + err);
});

module.exports = db;